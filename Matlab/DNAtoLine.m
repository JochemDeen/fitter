function DNA_Pos = DNAtoLine(points,X,Y,width)
%DNATOLINE Summary of this function goes here
%   Detailed explanation goes here
X=sort(X);
Y=sort(Y);

%create rectangle
[P11,P12,P21,P22]=create_rectangle(X(1),X(2),Y(1),Y(2),width);
        

%create polygon
polygonx=[P11(1) P12(1) P22(1) P21(1)];
polygony=[P11(2) P12(2) P22(2) P21(2)];

%check if gaussians within polygon
in=inpolygon(points(:,1), points(:,2), polygonx, polygony);
%add binary vector to list of molecules in the polygon
points=cat(2,in, points);
%Select only molecules that are in the polygon
pointsin=points(points(:,1)~=0,2:3);


%Create matrix with y-values
A=[pointsin(:,1),ones(size(pointsin(:,1),1),1)];
B=inv(A'*A)*A'*pointsin(:,2); %Ordinary least squares for least square

%create x-values for a line through the spots
%xx=min(pointsin(:,1)):0.1:max(pointsin(:,1));
%draw line using x-values and B(1), linear coefficient and B(2) offset.
%yy=B(1)*xx+B(2);
%line=[xx;yy];



%Get only the value paralel to the line running through all the spots
DNA_Pos=((pointsin(:,1)+B(2)/B(1))+pointsin(:,2)*B(1))/sqrt(1+B(1)^2); %dot product::  A dot B/ |B|

%create one dimensional data
DNA_Pos=sort(abs(DNA_Pos));
DNA_Pos=DNA_Pos-DNA_Pos(1);
end

