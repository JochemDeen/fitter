clc
clear


fp='D:\ADgut_measurement\20170120\Lambda_Alexa647_10mMMEA_GODCAT_640_030A-04ND_405_030_30ms_3\'; %filepath
fn='Thunderstorm.csv'; %filename

%Read Thunderstorm localization info
M = csvread([fp fn],1,0); % CSV read 
points=M(:,2:3)/100;


%Create gaussian image of points

%Initialization
xpixels=1000;
ypixels=1000;
image=zeros(xpixels,ypixels);
widthx=1.6;
widthy=1.6;

%Image construction
image=map_create_gaussian_image(points,image,widthx,widthy);

%% Localize DNA strands

f = figure(10);imagesc(image)
h = imline;
position = wait(h); 

    
pts.point1 = position(1,:); pts.point2 = position(2,:); pts.theta = 90-atan2d(pts.point2(2)-pts.point1(2),pts.point2(1)-pts.point1(1));

width=1;
DNA_Pos = DNAtoLine(points,pts.point1,pts.point2,width);

%% Create concencus map

%X=0:max(DNA_pos);
n=max(DNA_Pos)*10;
x = linspace(-1,max(DNA_Pos)+1,n);
sigma=0.2; %in pixels

%Create line profile of all locations
Line=sum(exp(-(ones(size(DNA_Pos,1),1)*x-DNA_Pos*ones(1,size(x,2))).^2/(2*sigma^2)));

%Generate derivative of the line profile
Lined=diff(Line); %(derivative)
xd=x(1:end-1);

%Find where the line crosses zero
Linedp=Lined;
Linedp(Linedp<0)=-1; %Create the jump of -2
Linedp(Linedp>0)=1; %Create the jump of -2

Linedpd=diff(Linedp); %2nd derivative

Found_jumps=find(Linedpd==-2); %Find the jump of -2
Conc_locations=x(Found_jumps)'; %Convert to an actual x-value


%Plot line and concencus line 
figure(100);cla;plot(x,Line./max(Line),'red');
%Mlist2=Mlist(SLine(Mlist)>threshold); %The amplitude of the peak needs to be above the threshold
Consensus=sum(exp(-(ones(size(Conc_locations,1),1)*x-Conc_locations*ones(1,size(x,2))).^2/(2*sigma^2)));
hold on
Line=plot(x,Consensus./max(Consensus),'green');
hold off
%legend('Remaining', 'Reference (norm.)','Correct');
set(Line,'LineSmoothing','on');
xlabel('Distance (pixels)');
axis([min(x) max(x) 0 1])
    