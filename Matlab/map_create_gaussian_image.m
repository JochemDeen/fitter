function image=map_create_gaussian_image(gaussians,image,widthx,widthy)
%Mask size
mask_size=widthy*5;
amplitude=1;

%This function adds gaussians to an image

%image is a zeros image on which we overlay the gaussians with 
imsize=[size(image,1),size(image,2)];

%note to self: Matrix(Row, Column)
%out=[frame, x, y, width x, width y, amp, back, total intensity, max of residual, mean of residual, std of residual];
%     1      2  3       4       5    6      7             8            9                   10                11

if size(gaussians,1)>0
    for i=1:1:size(gaussians,1) %Add gaussians found in 2 consecutive frames (within xx distance, see down).
        %Read the required fields
        xlocation=gaussians(i,1);
        ylocation=gaussians(i,2);
        
        %cropped image
        [xmin,xmax]=setminimal(xlocation,size(image,2),mask_size);
        [ymin,ymax]=setminimal(ylocation,size(image,1),mask_size);
        
        %Create a mesh grid
        [x,y]=meshgrid(xmin:xmax,ymin:ymax);
        %[x,y]=meshgrid(1:imsize(2),1:imsize(1)); 
        
        %add the function using the meshgrid to the image
        image(ymin:ymax,xmin:xmax)=image(ymin:ymax,xmin:xmax)+amplitude*exp(-((0.5*(x-xlocation).^2/widthx^2)+(0.5*(y-ylocation).^2/widthy^2)));
    end
end


function [min,max]=setminimal(location,max_size,mask_size)
min=floor(location-mask_size);
max=min+2*mask_size;
if min<1;min=1;end
if max>max_size;max=max_size;end