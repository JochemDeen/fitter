function [P11,P12,P21,P22]=create_rectangle(gx1,gx2,gy1,gy2,width)

%create rectangle
a1=0;a2=0;
if gy1<gy2; a1=gy2-gy1;elseif gy1~=gy2;a1=gy1-gy2;end
if gx1<gx2; a2=gx2-gx1;elseif gx1~=gx2;a2=gx1-gx2;end


b2=sqrt(a1^2*(width/2)^2/(a2^2+a1^2));
b1=sqrt(a2^2*(width/2)^2/(a1^2+a2^2));

if (gx2<=gx1 && gy2>=gy1) || (gx2>=gx1 && gy2<=gy1)
    P11=[gx1-b2 gy1-b1]; %(x,y)
    P12=[gx1+b2 gy1+b1];
    P21=[gx2-b2 gy2-b1];
    P22=[gx2+b2 gy2+b1];
elseif (gx2<gx1 && gy2<gy1) || (gx2>gx1 && gy2>gy1)
    P11=[gx1-b2 gy1+b1];
    P12=[gx1+b2 gy1-b1];
    P21=[gx2-b2 gy2+b1];
    P22=[gx2+b2 gy2-b1];            
end