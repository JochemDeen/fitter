function fitter_match(Data,Reference)

%Fitting parameters
method='LR_Stretch';        %fitting method. Possibilities 'LR_Stretch, LR, Simple
sigma=70;                   %localization inaccuracy (in bp)
delta=10;                   %number of consecutive false positives
tau=360;                    %mean fragment length (in basepairs) --> max(map)/size(map,1)
zeta=0.4;                   %false positives per kbp
theta=0.8;                  %labeling efficiency (between 0 and 1)
stretch=1.6;                %Stretching parameter (average stretch)
sigmastretch=0.1;          %variation of the stretching parameter (in absolute values)

Data_bp=Data/(0.34*stretch);

%set parameters depending on the method
if strcmp(method,'LR_Stretch')
    SP=[sigma;delta;tau;zeta;theta;sigmastretch];
elseif strcmp(method,'LR')
    SP=[sigma;delta;tau;zeta;theta];
elseif strcmp(method,'Simple')
	SP=[sigma;delta;zeta];
end

 %calculate the fitting
[Sites,Scores,Parameters]=Fitting(Data_bp,Reference,method,SP);
title('Score ')

%plot the matching map
plotscore_v2(Data_bp,Reference,Sites,Scores,Parameters(3))
title('Matching map ')


%calculate efficiency
sitesdata=Sites(1,:); %locations of the data are the 1st row

%Show the labeling efficiency based on the number of correctly matched
%sites
efficiency=size(sitesdata,2)/size(Data_bp,1);
disp(['efficiency: ', num2str(round(1000*efficiency)/10), '%'])

%Show the number of false positives based on the non-matched sites
labels=size(Data_bp,1);
disp(['extra labels: ' , num2str(labels-size(sitesdata,2)), ' or ' num2str(1000*(labels-size(sitesdata,2))/(Data_bp(end)-Data_bp(1))) ' per kbp' ])
