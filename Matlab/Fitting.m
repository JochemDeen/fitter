function [Sites,All_scores,Parameters]=Fitting(Data,Reference,method,SP)
%Calling the fitting function
[Fittingscores,All_sites,All_scores,Scoring_matrix]=SWFitter('fittoreference',Data,Reference,method,SP);
%which SWFitter
disp(['used: ', num2str(size(All_sites,2)), ' sites of the ' num2str(size(Data,1)), ' sites, with score:' num2str(Fittingscores(5)) ' and offset: ' , num2str(Fittingscores(3)) ' and direction: ' num2str(Fittingscores(4))])

max_offset=Fittingscores(3); %not used anymore, flawed parameter (17.05.2016)
score=Fittingscores(5);
Direction=Fittingscores(4);
number_used=size(All_sites,2);

%correct representations of all sites
locations=fliplr(All_sites)+1; %flip locations vector (note c++ starts from 0, not 1)

%the first matched site on reference and data
data_match1=Data(locations(1,1));
ref_match1=Reference(locations(2,1));

%difference between first site of reference and first matched site
refdiff=ref_match1-Reference(1);

%difference between first site of data and first matched site
datadiff=data_match1-Data(1);

%calculate offset when the first site is located at 0
max_offset=refdiff-datadiff+Reference(1);


Delta=SP(2); %Delta is 2nd parameter
%Calculate QS
%all possible scores for alignment on the entire molecule

if Delta>=size(Scoring_matrix,2)
    Delta=size(Scoring_matrix,2)-1;
end
last_scores=max(Scoring_matrix(:,end-Delta:end),[],2);
figure;
plot(Reference,last_scores);

%calculate background
background=median(last_scores);

%calculate Good_score=(score-back)^2
Good_score=(Fittingscores(5)-background)^2;
%calculate bad_score=mean((bad_score-back)^2)
bad_score=mean((last_scores-background).^2);
%calculate QS
QS=Good_score/bad_score;


if size(last_scores,1)>200;
    last_scores2=sort(last_scores);

    bad_score2=mean((last_scores2(end-100:end)-background).^2);

    QS2=Good_score/bad_score2;
else
    QS2=QS;
end

disp(['Quality score: ', num2str(QS2)])

%plot score
%plotscore_v2(Data,Reference,All_sites,All_scores,Fittingscores(4),imgname)

Sites=All_sites+1; %because matlab scoring is from 1
 
Parameters=[max_offset;score;Direction;number_used;0;QS2];
