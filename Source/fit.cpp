#ifndef FIT_H_INCLUDED
#include "fit.h"
#define FIT_H_INCLUDED
#endif // FIT_H_INCLUDED

//FitAlignmentController::FitAlignmentController(std::shared_ptr<map_read_collection> data_maps,std::shared_ptr<reference_read_collection> ref_maps,std::shared_ptr<scoring_params> sp){
//    data_maps=data_maps;
//    ref_maps=ref_maps;
//    sp=sp;
//}

FitAlignmentController::FitAlignmentController(map_read_collection new_data_maps,reference_read_collection new_ref_maps,scoring_params new_sp){
    data_maps=new_data_maps;
    ref_maps=new_ref_maps;
    sp=new_sp;

}

map_fits_container FitAlignmentController::DoFitAlignment(){
    int end_num = data_maps.collection.size();
    int ref_num= ref_maps.collection.size();
	if (verbose){
	mexPrintf("Running Alignment of  %d maps ", end_num);
	mexPrintf("vs %d references\n",ref_num);}

	if (end_num==0 || ref_num==0){
		mexErrMsgTxt("error no maps loaded");
	}

    //Initialization of all alignment parameters
    //vector<alignment_parameters_reference> alignment_list;
    //map_fits_container maps;
	if (verbose)
		mexPrintf("Initializing alignment parameters...");
    for (int i=0;i<end_num;i++){
        alignment_parameters_reference alignment_p;
        maps.alignment_list.push_back(alignment_p);
    }
		if (verbose)
		mexPrintf("done \n");


    for(int i=0; i<end_num; i++){
        std::vector <alignment_parameters_reference> temp_alignment;
        for (int j=0;j<ref_num;j++){
            alignment_parameters_reference alignment_t;
            temp_alignment.push_back(alignment_t);
        }
        for(int j=0; j<ref_num; j++){
            int index=i*(i-1)/2+j;
			if (verbose) mexPrintf("load map %d...",i );
            map_read_class for_map = data_maps.collection[i];
			if (verbose) mexPrintf("flip map %d...",i );
            map_read_class rev_map = for_map.reverse();
			if (verbose) mexPrintf("done\n");

            map_read_class reference_map = ref_maps.collection[j];

			
            rm_alignment for_alignment(for_map, reference_map, sp);
            rm_alignment rev_alignment(rev_map, reference_map, sp);
			
			
			if (verbose) mexPrintf("forward alignment...");
            for_alignment.fit_alignment(); //fast_fit_alignment();
			if (verbose) mexPrintf("done\n");
			if (verbose) mexPrintf("reverse alignment...");
            rev_alignment.fit_alignment(); //fast_fit_alignment();
			if (verbose) mexPrintf("done\n");

			if (verbose) mexPrintf("Assign results...");
            double for_score = for_alignment.Smax;
            double rev_score = rev_alignment.Smax;

            double offset;
            double t_Score;
            double Score;
            std::vector < std::vector <double> > Full_score_matrix;
            std::vector<double> all_scores;

            int map_ID,orientation,reference_ID;
            map_ID=j;
            vector<int> map_allsites;
			vector<int> ref_allsites;
            if (for_score>=rev_score){
                Score=for_score;
                orientation=1;
                int index_tar=for_alignment.tar_restr_al_sites.back();
                int index_ref=for_alignment.ref_restr_al_sites.back();
				//all_scores.reserve(for_alignment.all_scores.size());
				//copy(for_alignment.all_scores.begin(),for_alignment.all_scores.end(),back_inserter(all_scores));
                all_scores=for_alignment.all_scores;
				Full_score_matrix=for_alignment.S;
                offset=reference_map.map_read[index_ref];
                map_allsites=for_alignment.tar_restr_al_sites;
				ref_allsites=for_alignment.ref_restr_al_sites;
            }
            if (for_score<rev_score){
                Score=rev_score;
                orientation=-1;
                int index_tar=rev_alignment.tar_restr_al_sites.back();
                int index_ref=rev_alignment.ref_restr_al_sites.back();

                Full_score_matrix=rev_alignment.S;
                all_scores=rev_alignment.all_scores;

                offset=reference_map.map_read[index_ref];
                map_allsites=rev_alignment.tar_restr_al_sites;
				ref_allsites=rev_alignment.ref_restr_al_sites;
            }
            temp_alignment[j].setnew(Full_score_matrix,all_scores,Score,offset, map_ID, orientation, map_allsites,ref_allsites, j);
			if (verbose) mexPrintf("done\n");
        }
        int max_score;
        bool score_set=false;
        int maxj;
        for(int j=0; j<ref_num; j++){
            if (score_set==false){
                max_score=temp_alignment[j].Score;
                maxj=j;
                score_set=true;
            }
            else if (temp_alignment[j].Score>max_score){
                max_score=temp_alignment[j].Score;
                maxj=j;

            }
        }
        //maps.alignment_list[i].copyset(temp_alignment[maxj]);
		maps.alignment_list[i]=temp_alignment[maxj];
    }
    return maps;
}


vector<double> FitAlignmentController::DoBootstrap(int N, int sites){
    if (verbose) mexPrintf("Running bootstrap alignment\n");

	int end_num = data_maps.collection.size();
    int ref_num= ref_maps.collection.size();
	if (verbose) mexPrintf("Reading previous alignment...");

	alignment_parameters_reference alignment=maps.alignment_list[0];
	

	
	double Score=alignment.Score;
	int orientation=alignment.orientation;
	vector<int> All_sites_map=alignment.all_sites;
	reverse(All_sites_map.begin(),All_sites_map.end()); //reverse vector because begins at end

	vector<int> All_sites_ref=alignment.ref_all_sites;
	reverse(All_sites_ref.begin(),All_sites_ref.end());//reverse vector because begins at end

	vector<double> All_scores=alignment.all_scores;
	reverse(All_scores.begin(),All_scores.end());//reverse vector because begins at end
	if (verbose)  mexPrintf("alignment read\n");

	if (All_sites_ref.size()<2)
		mexErrMsgTxt("not enough aligned sites");


	if (verbose) mexPrintf("Creating bootstrap_params...");
	bootstrap_params bp(All_sites_map, All_sites_ref,All_scores, sites);
	if (verbose) mexPrintf("done\n");

	if (verbose) mexPrintf("Cutting reference maps...");
	map_read_class reference_map = ref_maps.collection[0];
	map_read_class new_reference_map=  reference_map.cutmap(bp.begin, bp.end);
	if (verbose) mexPrintf("shrunk from %d to %d\n", reference_map.map_read.size(), new_reference_map.map_read.size());

	assert(new_reference_map.map_read.size()>2);

	if (verbose) mexPrintf("Read orientation of data map...");
	map_read_class data_map;
	if (orientation==1){
		 data_map = data_maps.collection[0];
	}
	else{
		if (verbose) mexPrintf("flipping...");
		data_map = data_maps.collection[0].reverse();
	}

	//int map_size = data_map.map_read.size()-1; //-1 because x sites= x-1 overlaps
	//int ref_size = reference_map.map_read.size()-1;

	//Make bootstrap class
	if (verbose) mexPrintf("done\n");
	
	bootstrap_alignment bootstrapalignment(data_map,new_reference_map, sp, bp);

	double score;
	vector<double> Bootstrapscores;
	if (verbose)  mexPrintf("Start Bootstrap...");
	for (int i=0;i<N;i++){
		score=bootstrapalignment.bootstrap_fit_alignment();
		Bootstrapscores.push_back(score);
	}
	if (verbose) mexPrintf("done\n");

	return Bootstrapscores;
}
