#ifndef MAP_READ_H_INCLUDED
#include "map_read.h"
#endif // MAP_READ_H_INCLUDED


using namespace std;

map_read_class::map_read_class(string read, int ind){
  map_read.clear();

  //cout<<"map: "<<read<<endl;

  istringstream cur_stream(read);//(read);
  //cur_stream.str(read);
  while (cur_stream)
  {
      string string_value;
      if (!getline(cur_stream,string_value,',')) break;
	  double value=boost::math::iround(atof(string_value.c_str())*conv);
      map_read.push_back(value);
  }

  //cout<<"map loaded :" << map_read.size() << " sites."<<endl;
}

map_read_class map_read_class::reverse(){
  int i;
  vector<double> inv_map;
  double max_value=map_read.back();
  double map_value;
  for(i=map_read.size()-1; i>=0; i--){
        map_value=abs(map_read[i]-max_value);
    inv_map.push_back(map_value);
  }
    map_read_class new_read(inv_map);
  return new_read;
}

map_read_class map_read_class::cutmap(int begin, int end){
  vector<double> cutmap;
  double map_value;
  double beginmap_value=map_read[begin];
  
  for (int i=begin;i<=end;i++){
	  map_value=map_read[i]-beginmap_value;
	  cutmap.push_back(map_value);
  }

  map_read_class new_read(cutmap);
  return new_read;
}

map_read_class map_read_class::leaveout(vector<int> removesites){
  vector<double> cutmap;
  double map_value;
  assert(removesites.size()>0);

  int j=0;
  int I_rem=removesites[j];
  for (int i=0;i<map_read.size();i++){

	  if (!(i==I_rem)) {
		map_value=map_read[i];
	  cutmap.push_back(map_value);
		}
	  else{
		 
		  j++;
		  if (j<removesites.size()){
			I_rem=removesites[j];}
		};
  }

  map_read_class new_read(cutmap);
  return new_read;
}

map_read_class map_read_class::erasesites(vector<int> removesites){
  assert(removesites.size()>0);
   vector<double> newmap=map_read;

  for (int i=removesites.size()-1;i>=0;i--){
	  newmap.erase(newmap.begin()+removesites[i]);
  }
  map_read_class new_read(newmap);
  return new_read;
}


map_read_class::map_read_class(vector<double> &read){
map_read=read;
}

map_read_collection::map_read_collection(){
}//default constructor

map_read_collection::map_read_collection(ifstream& fstr){
  load(fstr);
}

map_read_collection::map_read_collection(mxArray *matlabArray){
	int M,N;
	double *A;

    M = mxGetM(matlabArray); /* Get the dimensions of A */
    N = mxGetN(matlabArray);
    A = mxGetPr(matlabArray); /* Get the pointer to the data of matlabArray */

    for (int n=0;n<N;n++){
        vector<double> cur_map;

        for(int m=0;m<M;m++){
            double value=A[m + M*n];
            if (value>=0){
                cur_map.push_back(value);}
        }

        map_read_class cur_read(cur_map); //creat map class out of vector
        collection.push_back(cur_read); //add current read to collection class
    }
}

void map_read_collection::load_matlabarray(mxArray *matlabArray){
	int M,N;
	double *A;

    M = mxGetM(matlabArray); /* Get the dimensions of A */
    N = mxGetN(matlabArray);
    A = mxGetPr(matlabArray); /* Get the pointer to the data of matlabArray */

	if ((N>1) && (M>1)){
				mexErrMsgTxt("Map must be a 1D vector.");
			}

	int maxvalue=max(M,N);
    
    vector<double> cur_map;

    for(int m=0;m<maxvalue;m++){
        double value=A[m]; // + M*n
        if (value>=0){
            cur_map.push_back(value);}
    }

	std::sort(cur_map.begin(), cur_map.end());
	for(int m=0;m<M-1;m++){
        if (cur_map[m]==cur_map[m+1]){
			mexErrMsgTxt("vector contains duplicates.");
		}

    }



        map_read_class cur_read(cur_map); //creat map class out of vector
        collection.push_back(cur_read); //add current read to collection class
}

void map_read_collection::load(ifstream& fstr){
  string line;
  int read_indexer = 0;

  while(fstr){
    if(!getline(fstr, line)) break;
    //if ((read_indexer % 100 == 0) && read_indexer>0) cout<<read_indexer<<" maps loaded"<<endl;
    map_read_class cur_read(line, read_indexer);
    collection.push_back(cur_read);
    read_indexer++;
    }
cout<< "using: "<< conv<< " basepairs/pixel"<<endl;
  cout<<"the number of maps loaded: "<<collection.size()<<endl;
  cout<<endl;
}

reference_read_collection::reference_read_collection(){}

reference_read_collection::reference_read_collection(mxArray *matlabArray){
	int M,N;
	double *A;

    M = mxGetM(matlabArray); /* Get the dimensions of A */
    N = mxGetN(matlabArray);
    A = mxGetPr(matlabArray); /* Get the pointer to the data of matlabArray */

	if ((N>1) && (M>1)){
				mexErrMsgTxt("Map must be a 1D vector.");
			}

	int maxvalue=max(M,N);
    
    vector<double> cur_map;

    for(int m=0;m<maxvalue;m++){
        double value=A[m]; // + M*n
        if (value>=0){
            cur_map.push_back(value);}
    }

	std::sort(cur_map.begin(), cur_map.end());
	for(int m=0;m<M-1;m++){
        if (cur_map[m]==cur_map[m+1]){
			mexErrMsgTxt("vector contains duplicates.");
		}

    }



        map_read_class cur_read(cur_map); //creat map class out of vector
        collection.push_back(cur_read); //add current read to collection class
}

reference_read_collection::reference_read_collection(ifstream& fstr){
	load_reference(fstr);
}


void reference_read_collection::load_reference(ifstream& fstr){
  string line;
  int read_indexer = 0;
  while(fstr){
if(!getline(fstr, line)) break;
    //if ((read_indexer % 100 == 0) && read_indexer>0) cout<<read_indexer<<" maps loaded"<<endl;
    map_read_class cur_read(line, read_indexer);
    collection.push_back(cur_read);
    read_indexer++;
    }
  cout<<"the number of references loaded: "<<collection.size()<<endl;
  cout<<endl;
}


void reference_read_collection::load_matlabarray(mxArray *matlabArray){
	int M,N;
	double *A;

    M = mxGetM(matlabArray); /* Get the dimensions of A */
    N = mxGetN(matlabArray);
    A = mxGetPr(matlabArray); /* Get the pointer to the data of matlabArray */

	if ((N>1) && (M>1)){
				mexErrMsgTxt("Map must be a 1D vector.");
			}

	int maxvalue=max(M,N);
    
    vector<double> cur_map;

    for(int m=0;m<maxvalue;m++){
        double value=A[m]; // + M*n
        if (value>=0){
            cur_map.push_back(value);}
    }

	std::sort(cur_map.begin(), cur_map.end());
	for(int m=0;m<M-1;m++){
        if (cur_map[m]==cur_map[m+1]){
			mexErrMsgTxt("vector contains duplicates.");
		}

    }

        map_read_class cur_read(cur_map); //creat map class out of vector
        collection.push_back(cur_read); //add current read to collection class
}