#include "mex.h"
#include "definitions.h"

#include <boost/algorithm/string.hpp>
#include <boost/smart_ptr.hpp>

//#include <iostream>
//#include <fstream>
//#include <string>
//#include <sstream>
//#include <iomanip>      // std::setprecision

#ifndef MAP_READ_H_INCLUDED
#include "map_read.h"
#endif // MAP_READ_H_INCLUDED

#ifndef SCORING_H_INCLUDED
#include "scoring.h"
#endif // SCORING_H_INCLUDED

#ifndef ALIGNMENT_H_INCLUDED
#include "Alignment.h"
#endif // ALIGNMENT_H_INCLUDED

#ifndef FIT_H_INCLUDED
#include "fit.h"
#endif // FIT_H_INCLUDED

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void fittoreference(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void overlapdata(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

std::string GetMatlabString(const mxArray* array);
