
#ifndef ALIGNMENT_H_INCLUDED
#include "Alignment.h"
#endif // ALIGNMENT_H_INCLUDED


using namespace std;

void rm_alignment::fast_overlap_alignment(){
    //double Distr_sigma=score_pars.Distr_sigma; //Width of gaussian
    //double Distr_lambda=score_pars.Distr_lambda; //Average length of fragment (currently unused)
    //double Mu=score_pars.Mu; //Penalty for unaligned site
    //int Delta=score_pars.Delta; //How many sites backwards

    int m = target_map.map_read.size()-1; //-1 because x sites= x-1 overlaps
    int n = ref_map.map_read.size()-1;
    int i, j, g, h;

    vector<double> q=target_map.map_read;
    vector<double> r=ref_map.map_read;
    //double score_thresh=25;
    //double t_score_thresh=8;

    Smax=0;

    ref_restr_al_sites.clear();
    tar_restr_al_sites.clear();

      fromi.clear();
      fromj.clear();

      T.clear();
      S.clear();

      vector<vector< bool > > mminf_matrix; //true if score is mminus inf

      for(i=0; i<=m; i++){
        vector<double> z;
        vector<int> mminone;
        vector<bool> bmminf;
        for(j=0; j<=n; j++){
          z.push_back(0);
          mminone.push_back(-1);
          bmminf.push_back(true);
        }
        fromi.push_back(mminone);
        fromj.push_back(mminone);
        S.push_back(z);
        T.push_back(z);
        mminf_matrix.push_back(bmminf);
      }

     //initialize S and inf matrix
      for(j=0; j<=n; j++){
        S[0][j] = 0;
        T[0][j] = 0;
        mminf_matrix[0][j] = false; //has been initialized
      }
      for(i=0; i<=m; i++){
        S[i][0] = 0;
        T[i][0] = 0;
        mminf_matrix[i][0] = false; //has been initialized
      }

      for(i=1; i<=m; i++){
        for(j=1; j<=n; j++){
            double y;
            double t_score;

          bool y_minf = mminf_matrix[i][j];
          for(g=max(0, i-score_pars.Delta); g<i; g++){
            for(h=max(0, j-score_pars.Delta); h<j; h++){
                if(mminf_matrix[g][h] == false){ //score has been set before
                    int map_gap  = i-g;
                    int ref_gap = j-h;
                    assert(ref_gap > 0);
                    assert(map_gap > 0);

                    double s = S[g][h] +
                      score_pars.score
                      (q[i]-q[g],r[j]-r[h],map_gap,ref_gap);
                    {if(y_minf == true){
                        y = s;
                        fromi[i][j] = g;
                        fromj[i][j] = h;
                        y_minf = false; //y has been set
                      }
                    else{
                        if(y < s){
                          fromi[i][j] = g;
                          fromj[i][j] = h;
                          y = s;	    //better choice of y
                        }
                    }}//end if
                    }// scores are set
            }
          }
      if(y_minf == false){
        if(fromi[i][j] != -1 && fromj[i][j] != -1){
        double cur_t_score = T[fromi[i][j]][fromj[i][j]] +
	    score_pars.site_score(i-fromi[i][j], j-fromj[i][j]);

	  ////double cur_t_score = T[fromi[i][j]][fromj[i][j]] +
	  ////  score_pars.site_opt_match_score(i-fromi[i][j], j-fromj[i][j]);
//    cout<<"current score: "<<y<<". Current T_score: "<< cur_t_score<<endl;
// cout<<"Threshold score: "<< score_thresh<< "T score thresh: "<<t_score_thresh<<endl;
	  if(y >= score_thresh && cur_t_score >= t_score_thresh)
        {

	    S[i][j] = y;
	    T[i][j] = cur_t_score;

	    mminf_matrix[i][j] = false;
	  }
	}
      }


    }}

    bool opt_score_minf = true;

   double Smmax;
   int immax = -1;
   int jmmax = -1;


    for(j=0; j<=n; j++){
    for(i=max(0, m-score_pars.Delta); i<=m; i++){
      if(mminf_matrix[i][j] == false){ //score has been set
	if(opt_score_minf == true){ //opt score not initialized
	  Smmax = S[i][j];
      immax = i;
	  jmmax = j;
	  opt_score_minf = false;
	}
	else{ //opt score already initialized
	  if(S[i][j] > Smmax){
	    Smmax = S[i][j];
	    immax = i;
	    jmmax = j;
	  }
	}
      }
    }
  }
  for(int i=0; i<=m; i++){
    for(int j=max(0, n-score_pars.Delta); j<=n; j++){
      if(mminf_matrix[i][j] == false){ //score has been set
	if(opt_score_minf == true){ //opt score not initialized
	  Smmax = S[i][j];
	  immax = i;
	  jmmax = j;
	  opt_score_minf = false;
	}
	else{ //opt score already initialized
	  if(S[i][j] > Smmax){
	    Smmax = S[i][j];
	    immax = i;
	    jmmax = j;
	  }
	}
      }
    }
  }
  assert(opt_score_minf == false);
  //end of optimal alignment search

  Smax = Smmax;
  Tmax = T[immax][jmmax];

    bool end_found = false;

  int curposi = immax;
  int curposj = jmmax;

  while(end_found == false){
    tar_restr_al_sites.push_back(curposi);
    ref_restr_al_sites.push_back(curposj);

    s_scores.push_back(S[curposi][curposj]);

    int newi;
    int newj;

    newi = fromi[curposi][curposj];
    newj = fromj[curposi][curposj];

    curposi = newi;
    curposj = newj;

    if(curposi == -1 || curposj == -1){
      end_found = true;
    }
  }

    }

void rm_alignment::fit_alignment(){

  int m = target_map.map_read.size()-1; //-1 because x sites= x-1 overlaps
  int n = ref_map.map_read.size()-1;
  int i, j, g, h, l;

  fromi.clear();
  fromj.clear();
  S.clear();
  Smax=0;

  ref_restr_al_sites.clear();
  tar_restr_al_sites.clear();

    vector<double> q=target_map.map_read;
    vector<double> r=ref_map.map_read;

    vector<vector< bool > > mminf_matrix; //true if score is mminus inf

      for(i=0; i<=m; i++){
        vector<double> z;
        vector<int> mminone;
        vector<bool> bmminf;
        for(j=0; j<=n; j++){
          z.push_back(0);
          mminone.push_back(-1);
          bmminf.push_back(true);
        }
        fromi.push_back(mminone);
        fromj.push_back(mminone);
        S.push_back(z);
        T.push_back(z);
        mminf_matrix.push_back(bmminf);
      }

     //initialize S and inf matrix
      for(j=0; j<=n; j++){
        S[0][j] = 0;
        T[0][j] = 0;
        mminf_matrix[0][j] = false; //has been initialized
      }
      for(i=0; i<=m; i++){
        S[i][0] = 0;
        T[i][0] = 0;
        mminf_matrix[i][0] = false; //has been initialized
      }

      for(i=1; i<=m; i++){
        for(j=1; j<=n; j++){
            double y;
            double t_score;

          bool y_minf = mminf_matrix[i][j];
          for(g=max(0, i-score_pars.Delta); g<i; g++){
            for(h=max(0, j-score_pars.Delta); h<j; h++){
                if(mminf_matrix[g][h] == false){ //score has been set before
                    int map_gap  = i-g;
                    int ref_gap = j-h;
                    assert(ref_gap > 0);
                    assert(map_gap > 0);


					double current_score= score_pars.fit_score
                      (q[i]-q[g],r[j]-r[h],map_gap,ref_gap);
					
                    double s = S[g][h] + current_score;
                      
					
					if(s>score_thresh_fit){
                    // {
                      if(y_minf == true){
                    y = s;
                    fromi[i][j] = g;
                    fromj[i][j] = h;
                    y_minf = false; //y has been set
                      }
                      else{
                    if(y < s){
                      fromi[i][j] = g;
                      fromj[i][j] = h;
                      y = s;	    //better choice of y
                    }
                      }
                    }// end if
                    }// scores are set
                }
            }// End of g//h loop
          S[i][j] = y;
          mminf_matrix[i][j] = y_minf;
          }
        }// end of fit computation

   bool opt_score_minf = true;
   double Smmax;
   int immax = -1;
   int jmmax = -1;


    for(j=0; j<=n; j++){
    for(i=max(0, m-score_pars.Delta); i<=m; i++){
      if(mminf_matrix[i][j] == false){ //score has been set
	if(opt_score_minf == true){ //opt score not initialized
	  Smmax = S[i][j];
      immax = i;
	  jmmax = j;
	  opt_score_minf = false;
	}
	else{ //opt score already initialized
	  if(S[i][j] > Smmax){
	    Smmax = S[i][j];
	    immax = i;
	    jmmax = j;
	  }
	}
      }
    }
  }
  for(int i=0; i<=m; i++){
    for(int j=max(0, n-score_pars.Delta); j<=n; j++){ //n-score_pars.Delta
      if(mminf_matrix[i][j] == false){ //score has been set
	if(opt_score_minf == true){ //opt score not initialized
	  Smmax = S[i][j];
	  immax = i;
	  jmmax = j;
	  opt_score_minf = false;
	}
	else{ //opt score already initialized
	  if(S[i][j] > Smmax){
	    Smmax = S[i][j];
	    immax = i;
	    jmmax = j;
	  }
	}
      }
    }
  }
  assert(opt_score_minf == false);
  //end of optimal alignment search

    Smax = Smmax;
    bool end_found = false;

  int curposi = immax;
  int curposj = jmmax;

    while(end_found == false){
      tar_restr_al_sites.push_back(curposi);
      ref_restr_al_sites.push_back(curposj);
      all_scores.push_back(S[curposi][curposj]);

      int newi;
      int newj;

      newi = fromi[curposi][curposj];
      newj = fromj[curposi][curposj];

      curposi = newi;
      curposj = newj;

      //cout<<"curposi:"<<curposi<<" curposj:"<<curposj<<endl;

      if(curposi == -1 || curposj == -1){
	end_found = true;
      }
    }

}

void rm_alignment::fast_fit_alignment(){

  int m = target_map.map_read.size()-1; //-1 because x sites= x-1 overlaps
  int n = ref_map.map_read.size()-1;
  int i, j, g, h, l;

  int im=m-score_pars.Delta;
  int jn=n-score_pars.Delta;


  fromi.clear();
  fromj.clear();
  S.clear();
  Smax=0;

  ref_restr_al_sites.clear();
  tar_restr_al_sites.clear();

vector<double> q=target_map.map_read;
vector<double> r=ref_map.map_read;

vector<vector< bool > > mminf_matrix; //true if score is mminus inf

    for(i=0; i<=m; i++){
    vector<double> z;
    vector<int> mminone;
    vector<bool> bmminf;
    for(j=0; j<=n; j++){
        z.push_back(0);
        mminone.push_back(-1);
        bmminf.push_back(true);
    }
    fromi.push_back(mminone);
    fromj.push_back(mminone);
    S.push_back(z);
    T.push_back(z);
    mminf_matrix.push_back(bmminf);
    }

    //initialize S and inf matrix
    for(j=0; j<=n; j++){
    S[0][j] = 0;
    T[0][j] = 0;
    mminf_matrix[0][j] = false; //has been initialized
    }
    for(i=0; i<=m; i++){
    S[i][0] = 0;
    T[i][0] = 0;
    mminf_matrix[i][0] = false; //has been initialized
    }

	//bool opt_score_minf = true;
	double Smmax=0;
	int immax = -1;
	int jmmax = -1;

      for(i=1; i<=m; i++){
        for(j=1; j<=n; j++){
            double y;
			int fromg,fromh;
            //double t_score;

          bool y_minf = mminf_matrix[i][j];

		  double qi=q[i];
		  double rj=r[j];

          for(g=max(0, i-score_pars.Delta); g<i; g++){
		  //for(g=i-1; g>=max(0, i-score_pars.Delta); g--){
			  double qg=q[g];
            for(h=max(0, j-score_pars.Delta); h<j; h++){
			  //for(h=j-1;h>=max(0, j-score_pars.Delta); h--){
				double rh=r[h];
                if(!mminf_matrix[g][h]){ //score has been set before
                    //int map_gap  = i-g;
                    //int ref_gap = j-h;
                    //assert(ref_gap > 0);
                    //assert(map_gap > 0);


					double current_score= score_pars.fit_score
                      (qi-qg,rj-rh,i-g,j-h);//map_gap,ref_gap 
					
					double s = S[g][h] + current_score;
					
					if(s>score_thresh_fit){
						
						
                    // {
                      if(y_minf){
                    y = s;
                    fromg = g;
                    fromh = h;
                    y_minf = false; //y has been set
                      }
                      else{
                    if(y < s){
                      fromg = g;
                      fromh = h;
                      y = s;	    //better choice of y
                    }
                      }
                    }// end if
                    }// scores are set
                }
            }// End of g//h loop
		  if(!y_minf){
          S[i][j] = y;
          mminf_matrix[i][j] = y_minf;
		  fromi[i][j] = fromg;
		  fromj[i][j] = fromh;

		  if (((i>im) || (j>jn))){
				  if(y > Smmax){
					Smmax = y;
					immax = i;
					jmmax = j;
				  }
		  }
		} //end if y_mind==false


          }
        }// end of fit computation

 //  bool opt_score_minf = true;
 //  double Smmax;
 //  int immax = -1;
 //  int jmmax = -1;


 //   for(j=0; j<=n; j++){
 //   for(i=max(0, m-score_pars.Delta); i<=m; i++){
 //     if(mminf_matrix[i][j] == false){ //score has been set
	//if(opt_score_minf == true){ //opt score not initialized
	//  Smmax = S[i][j];
 //     immax = i;
	//  jmmax = j;
	//  opt_score_minf = false;
	//}
	//else{ //opt score already initialized
	//  if(S[i][j] > Smmax){
	//    Smmax = S[i][j];
	//    immax = i;
	//    jmmax = j;
	//  }
	//}
 //     }
 //   }
 // }
 // for(int i=0; i<=m; i++){
 //   for(int j=max(0, n-score_pars.Delta); j<=n; j++){ //n-score_pars.Delta
 //     if(mminf_matrix[i][j] == false){ //score has been set
	//if(opt_score_minf == true){ //opt score not initialized
	//  Smmax = S[i][j];
	//  immax = i;
	//  jmmax = j;
	//  opt_score_minf = false;
	//}
	//else{ //opt score already initialized
	//  if(S[i][j] > Smmax){
	//    Smmax = S[i][j];
	//    immax = i;
	//    jmmax = j;
	//  }
	//}
 //     }
 //   }
 // }
  //assert(opt_score_minf == false);
  //end of optimal alignment search

    Smax = Smmax;
    bool end_not_found = true;

  int curposi = immax;
  int curposj = jmmax;

    while(end_not_found){
      tar_restr_al_sites.push_back(curposi);
      ref_restr_al_sites.push_back(curposj);
      all_scores.push_back(S[curposi][curposj]);

      int newi;
      int newj;

      newi = fromi[curposi][curposj];
      newj = fromj[curposi][curposj];

      curposi = newi;
      curposj = newj;

      //cout<<"curposi:"<<curposi<<" curposj:"<<curposj<<endl;

      if(curposi == -1 || curposj == -1){
	end_not_found = false;
      }
    }

}

double bootstrap_alignment::bootstrap_fit_alignment(){

  vector<int> removed_sites=Boots_pars.picksites();
  map_read_class new_refmap=ref_map.erasesites(removed_sites);;//leaveout(removed_sites);//erasesites(removed_sites); //

  int m = target_map.map_read.size()-1; //-1 because x sites= x-1 overlaps
  int n = new_refmap.map_read.size()-1;
  int i, j, g, h, l;

  int im=m-score_pars.Delta;
  int jn=n-score_pars.Delta;

  vector<vector<bool>> mminf_matrix=mminf_matrix_init;
  S=S_init;
  Smax=0;
  int begin=removed_sites[0];
  int begini,beginj;
  bool sitenotreached=true;
  double Smmax;

  int I=0;

  //Initialization in case begin=0;
  Smmax=0;
  S[0][0]=Smmax;
  mminf_matrix[0][0]=false; //score is set for this 1
  begini=0;beginj=0;

  while (sitenotreached){
	  if (I<Boots_pars.All_sites_map.size()){
	i=Boots_pars.All_sites_map[I];
	j=Boots_pars.All_sites_ref[I];

	if (j<begin){
		begini=i;
		beginj=j;
		Smmax=Boots_pars.All_scores[I];
		S[i][j]=Smmax;
		mminf_matrix[i][j]=false; //score is set for this 1
		I++;
	}else{
		sitenotreached=false;
	}}
	  else{
		  sitenotreached=false;
	  }
  }
  

  //for(j=0; j<=n; j++){
  //      S[0][j] = 0;
  //      mminf_matrix[0][j] = false; //has been initialized
  //    }
  //    for(i=0; i<=m; i++){
  //      S[i][0] = 0;
  //      mminf_matrix[i][0] = false; //has been initialized
  //    }
  //    begini=0;beginj=0;

     vector<double> q=target_map.map_read;
    vector<double> r=new_refmap.map_read;

	//assert(begini+1<=m);
	//assert(beginj+1<=n);

      for(i=begini+1; i<=m; i++){
		double qi=q[i];
        for(j=beginj+1; j<=n; j++){
          double y;
          bool y_minf = mminf_matrix[i][j];
		  double rj=r[j];

          for(g=max(begini, i-score_pars.Delta); g<i; g++){
			double qg=q[g];
            
			for(h=max(beginj, j-score_pars.Delta); h<j; h++){
				double rh=r[h];
                if(!mminf_matrix[g][h]){ //score has been set before
                    int map_gap  = i-g;
                    int ref_gap = j-h;
                    assert(ref_gap > 0);
                    assert(map_gap > 0);


					double current_score= score_pars.fit_score
                      (q[i]-q[g],r[j]-r[h],map_gap,ref_gap);                 
					
					if(current_score>score_pars.score_thresh_fit){
						
						double s = S[g][h] + current_score;
                    // {
                      if(y_minf){
                    y = s;
                    y_minf = false; //y has been set
                      }
                      else{
                    if(y < s){
                      y = s;	    //better choice of y
                    }
                      }
                    }// end if
                    }// scores are set
                }
            }// End of g//h loop
		  if(!y_minf){
          S[i][j] = y;
          mminf_matrix[i][j] = y_minf;
		 
		  if (((i>im) || (j>jn))){
				  if(y > Smmax){
					Smmax = y;
				  }
		  }
		} //end if y_mind==false


          }
        }// end of fit computation

    Smax = Smmax;
	
    return Smax;
}

rm_alignment::rm_alignment( map_read_class& tm,map_read_class& rm, scoring_params& sp){
  ref_map.map_read = rm.map_read;
  //ref_map.read_name = rm.read_name;
  target_map.map_read = tm.map_read;
  //target_map.read_name = tm.read_name;

  score_pars = sp;

  score_thresh_fit=sp.score_thresh_fit;
}

bootstrap_alignment::bootstrap_alignment(map_read_class& tm,map_read_class& rm, scoring_params& sp, bootstrap_params& bp){
  ref_map.map_read = rm.map_read;
  //ref_map.read_name = rm.read_name;
  target_map.map_read = tm.map_read;
  //target_map.read_name = tm.read_name;

  score_pars = sp;

  Boots_pars = bp;


  int m = target_map.map_read.size()-1; //-1 because x sites= x-1 overlaps
  int n = ref_map.map_read.size()-1-bp.Sites; //because map has less sites than original

  for(int i=0; i<=m; i++){
        vector<double> z;
        vector<int> mminone;
        vector<bool> bmminf;
        for(int j=0; j<=n; j++){
          z.push_back(0);
          mminone.push_back(-1);
          bmminf.push_back(true);
        }
        fromi_init.push_back(mminone);
        fromj_init.push_back(mminone);
        S_init.push_back(z);
        mminf_matrix_init.push_back(bmminf);
      }
}

alignment_parameters::alignment_parameters(){}

bootstrap_params::bootstrap_params(){}

bootstrap_params::bootstrap_params(vector<int> all_sites_map, vector<int> all_sites_ref, vector<double> all_scores, int sites){
	All_sites_map=all_sites_map;
	//All_sites_ref=all_sites_ref;

	begin=all_sites_ref[0];
	end=all_sites_ref.back();
	
	for (int i=0;i<all_sites_ref.size();i++){
		double site=all_sites_ref[i] - begin;
		All_sites_ref.push_back(site);
	}
	All_scores=all_scores;

	Sites=sites;

	
}

vector<int> bootstrap_params::picksites(){
	vector<int> random_numbers;
	vector<int> sites_remove;
	assert(Sites>0);

	int sizeofmap=end-begin;
	for (int i=0;i<=sizeofmap;i++){
		random_numbers.push_back(i);
	}

	random_shuffle(random_numbers.begin(),random_numbers.end());

	for (int i=0;i<Sites;i++){
		sites_remove.push_back(random_numbers[i]);
	}

	std::sort(sites_remove.begin(),sites_remove.end());

	return sites_remove;
}

alignment_parameters::alignment_parameters(vector<vector< double > > new_full_score, vector<double> new_all_scores,double new_Score,double new_offset, double new_t_Score, int new_map_ID_leftmap, int new_map_ID_rightmap, int new_left_orientation, int new_right_orientation, vector<int> new_left_all_sites, vector<int> new_right_all_sites){

Score=new_Score;
offset=new_offset;
map_ID_leftmap=new_map_ID_leftmap;
map_ID_rightmap=new_map_ID_rightmap;
left_orientation=new_left_orientation;
right_orientation=new_right_orientation;
left_all_sites=new_left_all_sites;
right_all_sites=new_right_all_sites;
t_Score=new_t_Score;
all_scores=new_all_scores;
Full_score_matrix=new_full_score;
}

void alignment_parameters::setnew(vector<vector< double > > new_full_score, vector<double> new_all_scores,double new_Score,double new_offset, double new_t_Score, int new_map_ID_leftmap, int new_map_ID_rightmap, int new_left_orientation, int new_right_orientation, vector<int> new_left_all_sites, vector<int> new_right_all_sites){

Score=new_Score;
offset=new_offset;
map_ID_leftmap=new_map_ID_leftmap;
map_ID_rightmap=new_map_ID_rightmap;
left_orientation=new_left_orientation;
right_orientation=new_right_orientation;
left_all_sites=new_left_all_sites;
right_all_sites=new_right_all_sites;
t_Score=new_t_Score;
all_scores=new_all_scores;
Full_score_matrix=new_full_score;
}

alignment_parameters_reference::alignment_parameters_reference(){}

alignment_parameters_reference::alignment_parameters_reference(vector<vector< double > > new_full_score, vector<double> new_all_scores,double new_Score,double new_offset, int new_map_ID, int new_orientation, vector<int> new_all_sites, vector<int> new_ref_all_sites, int new_reference_ID){
Score=new_Score;
offset=new_offset;
map_ID=new_map_ID;
orientation=new_orientation;
all_sites=new_all_sites;
ref_all_sites=new_ref_all_sites;
reference_ID=new_reference_ID;
all_scores=new_all_scores;
Full_score_matrix=new_full_score;
}

void alignment_parameters_reference::setnew(vector<vector< double > > new_full_score, vector<double> new_all_scores,double new_Score,double new_offset, int new_map_ID, int new_orientation, vector<int> new_all_sites, vector<int> new_ref_all_sites, int new_reference_ID){
Score=new_Score;
offset=new_offset;
map_ID=new_map_ID;
orientation=new_orientation;
all_sites=new_all_sites;
ref_all_sites=new_ref_all_sites;
reference_ID=new_reference_ID;
all_scores=new_all_scores;
Full_score_matrix=new_full_score;
}

void alignment_parameters_reference::copyset(alignment_parameters_reference templist){
Score=templist.Score;
offset=templist.offset;
map_ID=templist.map_ID;
orientation=templist.orientation;
all_sites=templist.all_sites;
ref_all_sites=templist.ref_all_sites;
reference_ID=templist.reference_ID;
all_scores=templist.all_scores;
Full_score_matrix=templist.Full_score_matrix;
}
