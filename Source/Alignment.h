#ifndef ALIGNMENT_H_INCLUDED
#define ALIGNMENT_H_INCLUDED

#ifndef __IOSTREAM__
#include <iostream>
#endif // __IOSTREAM__

#ifndef __SGI_STL_ALGORITHM
#include <algorithm>
#endif // __SGI_STL_ALGORITHM

#ifndef __CASSERT__
#include <assert.h>
#endif // __CASSERT__


#include <ctime>


#ifndef __FSTREAM__
#include <fstream>
#endif // __FSTREAM__


#ifndef MAP_READ_H_INCLUDED
#include "map_read.h"
#endif // MAP_READ_H_INCLUDED

#ifndef SCORING_H_INCLUDED
#include "scoring.h"
#endif // SCORING_H_INCLUDED

#ifdef WITH_MATLAB
#include "mex.h"
#endif // MAP_READ_H_INCLUDED

class rm_alignment{
private:
    //Used in overlap alignment
    static const int score_thresh=-100;
    static const int t_score_thresh=-5;

    //Used in fit alignment
    //static const int score_thresh_fit=-100;//-100;
    static const int t_score_thresh_fit=-5;
 public:
  map_read_class ref_map;
  map_read_class target_map;

 public:

	 int score_thresh_fit;

  vector<vector< double > > S; //total score alignment matrix
  vector<vector< double > > T; //t-score matrix

  vector<vector< int > > fromi;
  vector<vector< int > > fromj; //two traceback matrices

  vector<double> s_scores;
  vector<double> t_scores;

  vector< double > all_scores;

  double Smax; //maximum total alignment score
  double Tmax; //t-score of the optimal alignment

  scoring_params score_pars;

  vector< int > ref_restr_al_sites;
  //contains aligned restr. sites for ref. map
  vector< int > tar_restr_al_sites;
  //contains aligned restr. sites for tar. map

  //vector< double > declumped_best_scores;

 public:
  rm_alignment(map_read_class& rm, map_read_class& tm, scoring_params& sp);
  
  double al_ref_size();
  double al_tar_size();

  //void overlap_alignment();
  //void optimized_overlap_alignment();
  void fast_overlap_alignment();

  void fit_alignment(); //does a fit into the reference map
  void fast_fit_alignment(); //does a faster fit into the reference map

  //void optimized_fit_alignment();
  //void optimized_local_ref_alignment();
  //void optimized_local_alignment();

  //void fast_fit_alignment();
  //void localized_fit_alignment(int ref_left_ind, int ref_right_ind,
			       //int tar_left_ind, int tar_right_ind);
  //void fast_localized_fit_alignment(int ref_left_ind, int ref_right_ind,
				    //int tar_left_ind, int tar_right_ind);

  //void localized_gap_alignment(double gap_open, double gap_ext_kb,
			       //int ref_left_ind, int ref_right_ind,
			       //int tar_left_ind, int tar_right_ind);

  //void gap_alignment(double gap_open, double gap_ext_kb);

  //void fit_t_score();
  //void overlap_t_score();

  //double t_score_drop();

  //void output_kmers(ostream& out_str);
  //void output_alignment(ostream& out_str);

};

class bootstrap_params{

public:


	vector<int> All_sites_map;
	vector<int> All_sites_ref;
	vector<double> All_scores;

	int begin;
	int end;

	int Sites;

	bootstrap_params();
	bootstrap_params(vector<int> all_sites_map, vector<int> all_sites_ref, vector<double> all_scores, int sites);
	
	 vector< double > bootstrapscores;
	 vector<int> picksites();

};

class bootstrap_alignment{

public:
	
	 map_read_class ref_map;
	 map_read_class target_map;
	  vector<vector< double > > S; //total score alignment matrix

	  vector<vector< int > > fromi;
	  vector<vector< int > > fromj; //two traceback matrices

	  vector<double> s_scores;

	  vector< double > all_scores;

	  double Smax; //maximum total alignment score

	  scoring_params score_pars;
	  bootstrap_params Boots_pars;

	  vector< int > ref_restr_al_sites;
	  //contains aligned restr. sites for ref. map
	  vector< int > tar_restr_al_sites;
	  //contains aligned restr. sites for tar. map

	  
	 vector<vector< bool > > mminf_matrix_init;
	 vector<vector< double > > S_init; //total score alignment matrix

	  vector<vector< int > > fromi_init;
	  vector<vector< int > > fromj_init; //two traceback matrices

	  bootstrap_alignment(map_read_class& rm, map_read_class& tm, scoring_params& sp, bootstrap_params& bp);

	  double bootstrap_fit_alignment();

};

class alignment_parameters{

    public:
        double Score;
        double offset;
        double t_Score;

        int map_ID_leftmap;
        int map_ID_rightmap;
        int left_orientation;
        int right_orientation;

        vector< int > left_all_sites;
        vector< int > right_all_sites;

        vector< double > all_scores;

        vector<vector< double > > Full_score_matrix; //total score alignment matrix

        alignment_parameters();

        alignment_parameters(vector<vector< double > > full_score, vector<double> all_scores,double Score,double offset, double t_Score, int map_ID_leftmap, int map_ID_rightmap, int left_orientation, int right_orientation, vector<int> left_all_sites, vector<int> right_all_sites);

        void setnew(vector<vector< double > > new_full_score, vector<double> new_all_scores,double new_Score,double new_offset, double new_t_Score, int new_map_ID_leftmap, int new_map_ID_rightmap, int new_left_orientation, int new_right_orientation, vector<int> new_left_all_sites, vector<int> new_right_all_sites);



};



class alignment_parameters_reference{

    public:
        double Score;
        double offset;
        double t_Score;

        int map_ID;
        int orientation;
        int reference_ID;

        vector< int > all_sites;
		vector<int> ref_all_sites;

        vector< double > all_scores;
        vector<vector< double > > Full_score_matrix; //total score alignment matrix



        alignment_parameters_reference();

        alignment_parameters_reference(vector<vector< double > > full_score, vector<double> all_scores,double Score,double offset, int map_ID, int orientation, vector<int> all_sites, vector<int> ref_all_sites, int reference_ID);

        void setnew(vector<vector< double > > new_full_score, vector<double> new_all_scores,double new_Score,double new_offset, int new_map_ID, int new_orientation, vector<int> new_all_sites, vector<int> new_ref_all_sites, int new_reference_ID );

        void copyset(alignment_parameters_reference templist);

};
#endif // ALIGNMENT_H_INCLUDED
