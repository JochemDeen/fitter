#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

#ifndef __IOSTREAM__
#include <iostream>
#endif

#ifndef __CASSERT__
#include <cassert>
#endif

#ifndef DEFINITIONS_H_INCLUDED
#include "definitions.h"
#endif

#ifndef __CSTDIO__
#include <cstdio>
#endif


#ifndef __CMATH__
#include <cmath>
#endif

#ifndef __FSTREAM__
#include <fstream>
#endif

#ifndef __STRING__
#include <string>
#endif

#ifndef __SGI_STL_VECTOR
#include <vector>
#endif

#ifndef __SGI_STL_ALGORITHM
#include <algorithm>
#endif

#define Pi 3.14159265



#endif // FUNCTIONS_H_INCLUDED

double sqr(double x);
int sqr(int x);

double Gamma(int n);

double logGamma(int n);

double log_factorial(int n);

double log_poiss_pr(int x, double lambda);

double poiss_pr(int x, double lambda);

double poiss_p_value(int x, double lambda);

double log_n_choose_k(int n, int k);

double int_pow(double x, int p);

double falsepositive(double mx, double zeta, double y, double x);