#include "SW_fitter.h"

#define WITH_MATLAB




void mexFunction(int nlhs, mxArray *plhs[], /* Output variables */
    int nrhs, const mxArray *prhs[]) /* Input variables */{

    if (nrhs < 1)
        mexErrMsgTxt("First argument must be a string describing the operation to perform (\'fittoreference\', \'overlapdata\')");

    const mxArray* inputArray = prhs[0];
    if ((mxGetClassID(inputArray) != mxCHAR_CLASS) || (mxGetM(inputArray) > 1))
        mexErrMsgTxt("First argument must be a string describing the operation to perform (\'fittoreference\', \'overlapdata\')");

    std::string selector = GetMatlabString(inputArray);
	if (boost::iequals(selector, "fittoreference")) {
		fittoreference(nlhs, plhs, nrhs, prhs);
    } else if (boost::iequals(selector, "overlapdata")) {
		overlapdata(nlhs, plhs, nrhs, prhs);
    } else {
		mexErrMsgTxt("Unknown selector (should be one of \'fittoreference\', \'overlapdata\')");
	}

}

void fittoreference(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
   bool bootstrap=false;
   bool verbose=false;

   // check if verbose at last value is string
   const mxArray* inputArray = prhs[nrhs-1];
   if ((mxGetClassID(inputArray) == mxCHAR_CLASS) && (mxGetM(inputArray) == 1)){
	   std::string bootstrapstring = GetMatlabString(inputArray);
	   if (boost::iequals(bootstrapstring, "verbose")){
		   verbose=true;
			nrhs--; // subtract 1 from output because last is 'verbose' }
	   }
   }

	if (nrhs>=8){
	   const mxArray* inputArray = prhs[5];
	   if ((mxGetClassID(inputArray) != mxCHAR_CLASS) || (mxGetM(inputArray) > 1))
        mexErrMsgTxt("To run bootstrap, the 6th argument must be the string \'bootstrap\' , followed by the number of runs and the number of sites removed )");
	   std::string bootstrapstring = GetMatlabString(inputArray);
	   if (boost::iequals(bootstrapstring, "bootstrap")){
		   bootstrap=true;}
	   }
   
	
	if ((nrhs != 5) && (!bootstrap))
		mexErrMsgTxt("Must have exactly 5 input arguments for genome fitting\n\
					 SWfitter(method,data_map,reference_map, algorithm, scoring parameters) \n\
					 1 - the string \'fittoreference\'\n\
					 2. string containing the path to the data file, or a 2D matrix containing the mapping data\n\
					 3. string containing the path to the reference map, or a vector containing the reference map\n\
					 4. the overlap algorithm (\'simple\' or \'LR\' or \'LR_stretch\' or \'JD_norm\' or \'JD_exp\')\n\
					 5. a 2D matrix containing the values for the overlap algorithm, the format depends on the overlap algorithm see SW_fitter.m for details.\n\
					 Note: To run Bootstrap: SWfitter(method,data_map,reference_map, algorithm, scoring parameters,\'bootstrap\', N, S) \n\
					 N is the number of runs (int) and S, the number of sites removed (int).\n\
					 ");

	if ((nlhs < 1) || (nlhs> 4) && (!bootstrap))
		mexErrMsgTxt("incorrect number of output parameters, needed is: \n\
               1. Alignment parameters (required) \n\
               2. All_included sites for each map (optional) \n\
               3. The scores for each (optional) \n\
               4. Complete scoring matrix (optional, only for one map and one reference. ");

	if (!(nlhs==1) && (bootstrap))
		mexErrMsgTxt("incorrect number of output parameters. Bootstrap only gives 1 output parameter, a vector of doubles.");

	const mxArray* array;
	// the mxArray at index 0 will have been checked already by mexFunction

	// index 1 - must be the mapping data
	array = prhs[1];
	mxArray* dataArray = NULL;
	std::string filePathMap;
	int Nmaps,Mmaps;
	if (mxGetClassID(array) == mxCHAR_CLASS) {
		std::string filePathMap = GetMatlabString(array);
		if (filePathMap.empty())
			mexErrMsgTxt("Need a non-empty filepath to the data");
			mexPrintf("code under construction"); 
	} else {
		// assume that this is a valid data array
		// if it isn't then the image loader will throw an error
		dataArray = const_cast<mxArray*>(array);
		int N = mxGetN(dataArray);
		int M = mxGetM(dataArray);
		Mmaps=max(N,M);
	}

	// index 2 - must be the reference map
	array = prhs[2];
	mxArray* ReferenceArray = NULL;
	std::string filePathReference;
	int Nrefs,Mrefs;

	if (mxGetClassID(array) == mxCHAR_CLASS) {
		std::string filePathReference = GetMatlabString(array);
		if (filePathReference.empty())
			mexErrMsgTxt("Need a non-empty filepath to the genome map");
	} else {
		// assume that this is a valid data array
		// if it isn't then the image loader will throw an error
		int N = mxGetN(array); //Number of references
		int M= mxGetM(array); //number of values in Ref

		Mrefs=max(N,M);

		ReferenceArray=const_cast<mxArray*>(array);
	}

	// index 3 - must be a string, either 'LR' or 'simple' (not case sensitive)
	array = prhs[3];
	if (mxGetClassID(array) != mxCHAR_CLASS)
		mexErrMsgTxt("3rd argument must be a string (the overlap algorithm)");
	std::string segmentationStr = GetMatlabString(array);

	int OverlapAlgorithm;
	if (boost::iequals(segmentationStr, "LR")) {
		OverlapAlgorithm = OVERLAP_METHOD_LR;
	} else if (boost::iequals(segmentationStr, "Simple")) {
		OverlapAlgorithm = OVERLAP_METHOD_SIMPLE;
	} else if (boost::iequals(segmentationStr, "LR_STRETCH")) {
		OverlapAlgorithm = OVERLAP_METHOD_LR_STRETCHVAR;
	} else if (boost::iequals(segmentationStr, "JD_norm")) {
		OverlapAlgorithm = OVERLAP_METHOD_JD_NORM;
	}else if (boost::iequals(segmentationStr, "JD_exp")) {
		OverlapAlgorithm = OVERLAP_METHOD_JD_EXP;
	}else {
		mexErrMsgTxt("Unknown overlap algorithm");
	}

	// index 4 - must be a vector, with the values for the overlap algorithm
	array = prhs[4];
	mxArray* AlgorithmParameters;
	if ISREALDOUBLEVECTOR(array){
        AlgorithmParameters=const_cast<mxArray*>(array);
    }

	// if bootstrap, index 6 - must be an integer, with the values for the number of maps
	vector<double> Bootstrapscores;
	int N,sites;
	if (bootstrap){
	array = prhs[6];
	mxArray* value;
	if ISREALVALUE(array) {
        value=const_cast<mxArray*>(array);

		N=mxGetScalar(value);
    }
	}


	// if bootstrap, index 7 - must be an integer, with the values for the number of sites removed
	if (bootstrap){
	array = prhs[7];
	mxArray* value;
	if  ISREALVALUE(array){
        value=const_cast<mxArray*>(array);
		sites=mxGetScalar(value);
		if (!(sites>0)){
			mexErrMsgTxt("Need a positive number of sites to remove for bootstrapping");
		}
    }
	}

    //Load maps
    //std::shared_ptr<map_read_collection> maps;
    map_read_collection maps;
    if (!filePathMap.empty()) {
			ifstream ifs;
			ifs.open(filePathMap);
			maps.load(ifs);
            //std::shared_ptr<map_read_collection> maps(new map_read_collection(ifs));
            ifs.close();
    } else {
		maps.load_matlabarray(dataArray);
        //std::shared_ptr<map_read_collection> maps(new map_read_collection(dataArray));
    }

    //Load reference map
    //std::shared_ptr<reference_read_collection> ref_maps;
    reference_read_collection ref_maps;
    if (!filePathReference.empty()) {
			ifstream rfs;
			rfs.open(filePathReference);
			ref_maps.load_reference(rfs);
            //std::shared_ptr<reference_read_collection> ref_maps(new reference_read_collection(rfs));
            rfs.close();
    } else {
        ref_maps.load_matlabarray(ReferenceArray);
        //std::shared_ptr<reference_read_collection> ref_maps(new reference_read_collection(ReferenceArray));
    }
	
    //Set fitting parameters
    scoring_params sp(OverlapAlgorithm,AlgorithmParameters);
    //std::shared_ptr<scoring_params> sp(new scoring_params(OverlapAlgorithm,AlgorithmParameters));

	
    //Create the fit Alignment controller
    FitAlignmentController Fit_Alignment(maps,ref_maps,sp);
	Fit_Alignment.verbose=verbose;
    //std::shared_ptr<FitAlignmentController> Fit_Alignment( new FitAlignmentController(maps,ref_maps,sp));

    //Run the allignment
    map_fits_container map_fits=Fit_Alignment.DoFitAlignment();

	if (bootstrap){
		Bootstrapscores=Fit_Alignment.DoBootstrap(N,sites);
	}

    //Save the values

    //Size of matrices
    size_t number_of_maps=maps.collection.size();
    size_t number_of_refs=ref_maps.collection.size();
    size_t M=number_of_maps;

    //Output 1 is fits data
    if ((nlhs>=1) && !bootstrap){
        mxArray* outputArray = mxCreateDoubleMatrix(number_of_maps, 5, mxREAL);
        double* outputPointer1 = mxGetPr(outputArray);

        for(int m=0;m<number_of_maps;m++){

            outputPointer1[m+M*0]=m;
            outputPointer1[m+M*1]=map_fits.alignment_list[m].reference_ID;
            outputPointer1[m+M*2]=map_fits.alignment_list[m].offset;
            outputPointer1[m+M*3]=map_fits.alignment_list[m].orientation;
            outputPointer1[m+M*4]=map_fits.alignment_list[m].Score;
        }
        plhs[0] = outputArray;
    }

	//if bootstrap, output is only a vector of scores;
	if (bootstrap){
		int bootstrap_size=Bootstrapscores.size();
		mxArray* outputArray = mxCreateDoubleMatrix(1, bootstrap_size, mxREAL);
        double* outputPointer1 = mxGetPr(outputArray);

		for(int i=0;i<bootstrap_size;i++){

		outputPointer1[i]=Bootstrapscores[i];}
		plhs[0] = outputArray;

	}


    //Output 2 is localization data, only for one map loaded
    if (nlhs>=2 && number_of_maps==1 && !bootstrap){
		int map_size=map_fits.alignment_list[0].all_sites.size();
            
        mxArray* outputArray2 = mxCreateDoubleMatrix(2, map_size, mxREAL);
        double* outputPointer2 = mxGetPr(outputArray2);
		
        for(int m=0;m<number_of_maps;m++){
            for (int n=0;n<map_size;n++){
            outputPointer2[0+2*n]=map_fits.alignment_list[m].all_sites[n];
            }
			int m2=1;
			for (int n=0;n<map_size;n++){
            outputPointer2[1+2*n]=map_fits.alignment_list[m].ref_all_sites[n];
            }
        }
        plhs[1] = outputArray2;
    }


    //Output 3 is score for each fragment
    if (nlhs>=3 && !bootstrap){
		int map_size=map_fits.alignment_list[0].all_scores.size();
        mxArray* outputArray3 = mxCreateDoubleMatrix(number_of_maps, map_size, mxREAL);
        double* outputPointer3 = mxGetPr(outputArray3);
        for(int m=0;m<number_of_maps;m++){
            
            for (int n=0;n<map_size;n++){
            outputPointer3[m+M*n]=map_fits.alignment_list[m].all_scores[n];
            }
			
        }
        plhs[2] = outputArray3;
    }


    //output 4, full_scoring matrix, only if only one map and only 1 reference
    if (nlhs==4 && number_of_maps==1 && number_of_refs==1 && !bootstrap){
        mxArray* outputArray4 = mxCreateDoubleMatrix(Mrefs, Mmaps, mxREAL);
        double* outputPointer4 = mxGetPr(outputArray4);

        for(int m=0;m<Mmaps;m++){

            for (int n=0;n<Mrefs;n++){
            outputPointer4[n+Mrefs*m]=map_fits.alignment_list[0].Full_score_matrix[m][n];
            }
        }
        plhs[3] = outputArray4;
    }



}

void overlapdata(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){



}

std::string GetMatlabString(const mxArray* array) {
	char *str = NULL;
	str = mxArrayToString(array);
	if (str == NULL)
		mexErrMsgTxt("Error: cannot allocate string (non-string argument supplied?)");
	std::string stdStr(str);
	mxFree(str);
	return stdStr;
}
