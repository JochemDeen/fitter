#ifndef FIT_H_INCLUDED
#define FIT_H_INCLUDED
#endif // FIT_H_INCLUDED


#ifndef ALIGNMENT_H_INCLUDED
#include "Alignment.h"
#endif // ALIGNMENT_H_INCLUDED

#ifdef WITH_MATLAB
#include "mex.h"
#endif

#include <vector>
#include <algorithm>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <iterator>


class map_fits_container{

private:

public:
vector<alignment_parameters_reference> alignment_list;

};

class FitAlignmentController{

private:

public:

map_read_collection data_maps;
reference_read_collection ref_maps;
scoring_params sp;
map_fits_container maps;
//std::shared_ptr<map_fits_container> map_fits;
bool verbose;


//FitAlignmentController(std::shared_ptr<map_read_collection> data_maps,std::shared_ptr<reference_read_collection> ref_maps,std::shared_ptr<scoring_params> sp);
FitAlignmentController(map_read_collection new_data_maps,reference_read_collection new_ref_maps,scoring_params new_sp);

//vector<double> Bootstrapscores;

map_fits_container DoFitAlignment();
vector<double> DoBootstrap(int N, int sites);


};


