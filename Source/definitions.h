#include "mex.h"

#ifndef DEFINITIONS_H_INCLUDED
#define DEFINITIONS_H_INCLUDED

const int OVERLAP_METHOD_LR=0;
const int OVERLAP_METHOD_SIMPLE=1;
const int OVERLAP_METHOD_LR_STRETCHVAR=2;
const int OVERLAP_METHOD_JD_NORM=3;
const int OVERLAP_METHOD_JD_EXP=4;

#define ISREALDOUBLEVECTOR(X) (!mxIsComplex(X) && mxGetNumberOfDimensions(X) == 2 && !mxIsSparse(X) && mxIsDouble(X) && mxGetN(X)==1);

#define ISREALVALUE(X) (!mxIsComplex(X) && !mxIsEmpty(X) && mxIsNumeric(X) && mxGetNumberOfElements(X) == 1);

#endif // DEFINITIONS_H_INCLUDED
