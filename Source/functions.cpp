#ifndef FUNCTIONS_H_INCLUDED
#include "functions.h"
#endif // FUNCTIONS_H_INCLUDED

#define pi 3.14156

//double square(double x){
//  return x*x;
//}
double sqr(double x){
  return x*x;
}

int sqr(int x){
  return x*x;
}


double Gamma(int n){
  //calculates factorial
  assert(n>=1);
  double max_prec_n = 11;
  if(n<=max_prec_n){
    switch(n){
    case 1: return 1;
    case 2: return 1;
    case 3: return 2;
    case 4: return 6;
    case 5: return 24;
    case 6: return 120;
    case 7: return 720;
    case 8: return 5040;
    case 9: return 40320;
    case 10: return 362880;
    case 11: return 3628800;
    }
  }
  assert(n<=11);
}

double logGamma(int n){
  //calculates log factorial
  assert(n>=1);

  int max_prec_n = 11;

  if(n<=max_prec_n){
    switch(n){
    case 0: assert(false);
    case 1: return 0;
    case 2: return 0;
    case 3: return 0.693147181;
    case 4: return 1.791759469;
    case 5: return 3.17805383;
    case 6: return 4.787491743;
    case 7: return 6.579251212;
    case 8: return 8.525161361;
    case 9: return 10.6046029;
    case 10: return 12.80182748;
    case 11: return 15.10441257;
    }
  }
  else{
    double sum = 0;
    int i;
    for(i=2; i<n; i++){
      double di;
      di = (double)i;

      sum += log(di);
    }
    return sum;
  }
}

double log_factorial(int n){
  return logGamma(n+1);
}
double log_poiss_pr(int x, double lambda){
  assert(lambda > 0);
  assert(x >= 0);

  double log_pr;
  log_pr = -lambda + x*log(lambda)-logGamma(x+1);

  assert(log_pr <= 0);

  return log_pr;
}
double poiss_pr(int x, double lambda){
  return exp(log_poiss_pr(x, lambda));
}

double poiss_p_value(int x, double lambda){
  double max_iterations = 15;
  int it = 0;
  double epsilon = 0.000001;
  //precision
  double cur_diff = 1;
  double p_value = 0;

  int cur_x = x;

  double cur_pr = 0;
  double last_pr = 10;
  while (cur_diff > epsilon){
    //while(it<= max_iterations){
    it++;
    cur_pr = poiss_pr(cur_x, lambda);


    p_value += cur_pr;
    cur_diff = fabs(cur_pr-last_pr);
    last_pr = cur_pr;
    cur_x++;
  }

  //cout<<"iterations: "<<it<<endl;
  return p_value;
}

double log_n_choose_k(int n, int k){
  if(k==0) return 0;
  else{
    assert(k<=n);
    assert(k>=1);

    double log_n_fact = 0;
    double log_k_fact = 0;
    double log_nmink_fact = 0;

    for(int i=1; i<=n; i++){
      log_n_fact += log((double)i);
    }
    for(int i=1; i<=k; i++){
      log_k_fact += log((double)i);
    }
    for(int i=1; i<=n-k; i++){
      log_nmink_fact += log((double)i);
    }
    return log_n_fact - log_k_fact - log_nmink_fact;
  }
}


double int_pow(double x, int p){
  double res = 1;
  if (p==0) return res;
  int n;
  if(p>0) n = p;
  if(p<0) n = -p;

  int i;
  for(i=1; i<=p; i++){
    res*= x;
  }
  if (p>0) return res;
  return (1/res);
}


double falsepositive(double mx, double zeta, double y, double x){

if (mx==1){
	return 0;}
else
{
	if (!(y>0)){
		mexPrintf("y:%f\n",y);
		mexPrintf("zeta:%f\n",zeta);
	}
	assert(zeta >0 & y>0);
	return (mx-1)*log(y*zeta)-(mx-1)*log(x) ;}
	}
