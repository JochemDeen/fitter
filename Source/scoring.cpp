#ifndef SCORING_H_INCLUDED
#include "scoring.h"
#endif // SCORING_H_INCLUDED

using namespace std;

scoring_params::scoring_params(){}

scoring_params::scoring_params(ifstream& fstr){
    string line;
    vector <string> parameters;

    if (fstr.is_open()){
     while(fstr.good()){//(!getline(fstr, line).eof() ){

        getline(fstr, line);//empty line
        size_t equalsign = line.find("=");      // position of "live" in str
        string param=line.substr(0,equalsign);
        string value=line.substr(equalsign+1);
        set_parameter(param,value);

        parameters.push_back(line);
    }
    number_of_parameters=parameters.size();
  cout<<"the number of scoring parameters: "<<number_of_parameters<<endl;
    }
}

scoring_params::scoring_params(int method,mxArray *matlabArray){
	int M,N;
	double *A;
	Method=method;

    M = mxGetM(matlabArray); /* Get the dimensions of A */
    N = mxGetN(matlabArray);
    A = mxGetPr(matlabArray); /* Get the pointer to the data of matlabArray */

    if (N>1){
        mexErrMsgTxt("the parameters for the fitting algorithm must be a 1D vector");
    }
    if (method==OVERLAP_METHOD_LR){
        if (M!=5){
        mexErrMsgTxt("Must have exactly 5 input parameters for the LR fitting algorithm:\n\
                     1. Distr_sigma (localization error) \n\
					 2. Delta (number of consecutive false positives, default 7)\n\
					 3. Tau (Poisson distribution average length)\n\
					 4. Zeta (number of false positives per kbp)\n\
					 5. Theta (Labeling efficiency)\n\
                     ");}
        Distr_sigma=double(A[0]);
        Delta=A[1]+1;
        Tau=double(A[2]);
        Zeta=double(A[3]);
        Theta=double(A[4]);
		Zeta=Zeta/1000;
		if (Delta>1 && Zeta<=0)
		{mexErrMsgTxt("cannot have a probability of 0 false positives and more than 0 consecutive false positives");}
		setconstants();
		score_thresh_fit=-100;
    }
    if (method==OVERLAP_METHOD_SIMPLE){
        if (M!=3){
        mexErrMsgTxt("Must have exactly 3 input parameters for the simple fitting algorithm:\n\
                     1. Distr_sigma (localization error) \n\
					 2. Mu (penalty)\n\
					 3. Delta (number of consecutive false positives)\n\
                     ");}
    Distr_sigma=A[0];
    Mu=A[1];
    Delta=A[2];
	score_thresh_fit=-100;
    }
	if (method==OVERLAP_METHOD_LR_STRETCHVAR){
        if (M!=6){
        mexErrMsgTxt("Must have exactly 5 input parameters for the LR fitting algorithm:\n\
                     1. Distr_sigma (localization error) \n\
					 2. Delta (number of consecutive false positives, default 7)\n\
					 3. Tau (Poisson distribution average length)\n\
					 4. Zeta (number of false positives per kbp)\n\
					 5. Theta (Labeling efficiency)\n\
					 6. Stretching variation (between 0 and 1) \n\
                     ");}
        Distr_sigma=double(A[0]);
        Delta=A[1]+1;
        Tau=double(A[2]);
        Zeta=double(A[3]);
        Theta=double(A[4]);
		Zeta=Zeta/1000;
		Stretch_var=double(A[5]);
		if (Delta>1 & Zeta<=0)
		{mexErrMsgTxt("cannot have a probability of 0 false positives and more than 0 consecutive false positives");}
		setconstants();
		score_thresh_fit=-100;
	}
	if (method==OVERLAP_METHOD_JD_NORM){
        if (M!=5){
        mexErrMsgTxt("Must have exactly 5 input parameters for the LR fitting algorithm:\n\
                     1. Distr_sigma (localization error) \n\
					 2. Delta (number of consecutive false positives, default 7)\n\
					 3. Tau (Poisson distribution average length)\n\
					 4. Zeta (number of false positives per kbp)\n\
					 5. Theta (Labeling efficiency)\n\
                     ");}
        Distr_sigma=double(A[0]);
        Delta=A[1]+1;
        Tau=double(A[2]);
        Zeta=double(A[3]);
        Theta=double(A[4]);
		Zeta=Zeta/1000;
		if (Delta>1 & Zeta<=0)
		{mexErrMsgTxt("cannot have a probability of 0 false positives and more than 0 consecutive false positives");}
		setconstants();
		score_thresh_fit=-100;
    }
	if (method==OVERLAP_METHOD_JD_EXP){
        if (M!=5){
        mexErrMsgTxt("Must have exactly 5 input parameters for the LR fitting algorithm:\n\
                     1. Distr_sigma (localization error) \n\
					 2. Delta (number of consecutive false positives, default 7)\n\
					 3. Tau (Poisson distribution average length)\n\
					 4. Zeta (number of false positives per kbp)\n\
					 5. Theta (Labeling efficiency)\n\
                     ");}
        Distr_sigma=double(A[0]);
        Delta=A[1]+1;
        Tau=double(A[2]);
        Zeta=double(A[3]);
        Theta=double(A[4]);
		Zeta=Zeta/1000;
		if (Delta>1 & Zeta<=0)
		{mexErrMsgTxt("cannot have a probability of 0 false positives and more than 0 consecutive false positives");}
		setconstants();
		
		score_thresh_fit=-100;
    }
}

void scoring_params::stringToUpper(string &s)
{
   for(unsigned int l = 0; l < s.length(); l++)
  {
    s[l] = toupper(s[l]);
  }
}

void scoring_params::stringToLower(string &s)
{
   for(unsigned int l = 0; l < s.length(); l++)
  {
    s[l] = tolower(s[l]);
  }
}

void scoring_params::set_parameter(string param,string value){
    cout<<param<<"="<<value<<endl;
    stringToUpper(param);
if (param=="DISTR_SIGMA"){
        Distr_sigma=atof(value.c_str());
       }
if (param=="DISTR_LAMBDA"){
        Distr_lambda=atof(value.c_str());
        }
if (param=="MU"){
        Mu=atof(value.c_str());
        }
if (param=="DELTA"){
        Delta=atoi(value.c_str());
        }
if (param=="TAU"){
        Tau=atoi(value.c_str());
        }
if (param=="ZETA"){
        Zeta=atoi(value.c_str());//convert per kbp to per bp
        Zeta=Zeta/1000;
        }
if (param=="THETA"){
        Theta=atof(value.c_str());
        }
if (param=="METHOD"){
        string method_string=value.c_str();
        stringToLower(method_string);
        method=method_string;
        cout<<method<<endl;
        }
}

void scoring_params::check_parameters(){
if (!((method=="simple") | (method=="lr"))){
    cerr<<"Incorrect method"<<endl;
    assert(false);
}
if (!((method=="simple") & (Distr_sigma>0) & (Mu>0) & (Delta>0))){
cerr<<"Insufficient parameters:"<<endl;
    cerr<<"Simple method requires Distr_sigma (error), Mu (penalty), Delta (number of consecutive false positives)"<<endl;
    assert(false);}
if (!((method=="lr") & (Distr_sigma>0) & (Mu>0) & (Delta>0) & (Tau>0))){
cerr<<"Insufficient parameters:"<<endl;
    cerr<<"Likelyhood relation method requires Distr_sigma (error), Mu (penalty, default=1.2), Delta (number of consecutive false positives), Tau (Poisson distribution average length)"<<endl;
    assert(false);}

}

void scoring_params::check_parameters_fit(){
if (!(method=="lr")){
    cerr<<"Incorrect method: "<<method<<endl;
    assert(false);}
if (!((method=="lr") & (Distr_sigma>0) & (Delta>0) & (Tau>0) &(Zeta>=0) & (Theta>0))){
    cout<<Distr_sigma<<","<<Delta<<","<<Tau<<","<<Zeta<<","<<Theta<<endl;
    cerr<<"Insufficient parameters:"<<endl;
    cerr<<"Likelyhood relation method requires Distr_sigma (error), Delta (number of consecutive false positives, default 7), Tau (Poisson distribution average length), Zeta (number of false positives per kbp), Theta (Labeling efficiency)"<<endl;
    assert(false);}
    else{
        setconstants();
    }
}

void scoring_params::setconstants(){
 c1=log(sqrt(Pi*Distr_sigma)/Delta);
 if (Theta>0.99){
        c2=-2;}
 else{c2=log(1-Theta);}
 c3=log(sqrt(Pi*Distr_sigma));

 logtau=log(Tau);
 tauzeta=Tau*Zeta;
 tauzeta_=1/tauzeta;
 twosigma2=2*sqr(Distr_sigma);

 if (Method==OVERLAP_METHOD_JD_NORM){
 
	double score= 0; //false positive for 0 skips
	missites.push_back(score);

	for (int i=1;i<=Delta;i++){
		score=i*c2;
		missites.push_back(score);
	}
 }
}


double scoring_params::score(double x1, double x2, int m1, int m2){
	assert(x1>0);
	assert(x2>0);

if (method=="simple"){
    return opt_size_score_simple(x1, x2, m1, m2);
}
if (method=="lr"){
    return LR_size_score(x1, x2, m1, m2);
}
if (method=="lr_stretch"){
	 return LR_stretch_size_score(x1, x2, m1, m2);
}
if (method=="jd_norm"){
	 return JD_norm_score(x1, x2, m1, m2);
}
if (method=="jd_exp"){
	 return JD_exp_score(x1, x2, m1, m2);
}

}

double scoring_params::fit_score(double x, double y, int m_x, int m_y){
assert(x > 0);
assert(y > 0);
assert(m_x > 0);
assert(m_y > 0);

if (Method==OVERLAP_METHOD_SIMPLE){
    return opt_fit_score_simple(x, y, m_x, m_y);
}
if (Method==OVERLAP_METHOD_LR){
    return LR_fit_score(x, y, m_x, m_y);
}
if (Method==OVERLAP_METHOD_LR_STRETCHVAR){
	 return LR_stretch_fit_score(x, y, m_x, m_y);
}
if (Method==OVERLAP_METHOD_JD_NORM){
	 return JD_norm_score(x, y, m_x, m_y);
}
if (Method==OVERLAP_METHOD_JD_EXP){
	 return JD_exp_score(x, y, m_x, m_y);
}

}

double scoring_params::opt_fit_score_simple(double x, double y, int m_x, int m_y){
if (abs(x-y)>3*Distr_sigma)
{
    double score=-1-Mu*(m_x-1)-(m_y-1)*Mu;
    return score;
}


double score=-1+2*exp(-pow((x-y),2)/(2*pow(Distr_sigma,2)))-Mu*(m_x-1)-(m_y-1)*Mu;
return score;
}





double scoring_params::LR_fit_score(double x, double y, int m_x, int m_y){
if (abs(x-y)>3*Distr_sigma) return -100;

double score;

double fp=falsepositive(m_x,Zeta, y, x);//(m_x-1)*log(y*Zeta)

score=(m_y-1)*c2 - c1+ fp- Zeta*y

    + m_x*logtau + x/Tau
    -sqr(x-y)/(twosigma2);
return score;
}

double scoring_params::LR_stretch_fit_score(double x, double y, int m_x, int m_y){
if (abs(x-y)>3*(Distr_sigma+sqrt(Stretch_var*x))) return -100;

double score;

double fp=falsepositive(m_x,Zeta, y,x);//(m_x-1)*log(y*Zeta)

score=(m_y-1)*c2 - c1+ fp - Zeta*y

    + m_x*logtau + x/Tau
    -sqr(x-y)/(twosigma2+Stretch_var*x);
return score;
}

double scoring_params::JD_norm_score(double x, double y, int m_x, int m_y){
if (abs(x-y)>3*Distr_sigma) return -100;

//double size = x/Tau;
//double distance = -sqr(x-y)*(1/twosigma2);
//
//double fp_slope = tauzeta_/(y*Zeta);
//double fp = -(m_x-1)*fp_slope+tauzeta_;
//
//double miss = -(m_y-1) * c2;

//double score = size + distance + fp + miss;
double fp;
if (m_x==1){
	fp=0;}
else{
	fp = (m_x-1)* tauzeta_/(y*Zeta);
}


double score = x/Tau - fp +tauzeta_- missites[m_y-1] -sqr(x-y)/twosigma2;

return score;
}


double scoring_params::JD_exp_score(double x, double y, int m_x, int m_y){
if (abs(x-y)>3*Distr_sigma) return -100;
//
//double size = x/Tau;
//double distance = exp(-sqr(x-y)*(1/twosigma2));
//
//double fp_slope = tauzeta_/(y*Zeta);
//double fp = -(m_x-1)*fp_slope+tauzeta_;
//
//double miss = -(m_y-1) * c2;

//double score = size * distance + fp + miss;

double score = x/Tau * exp(-sqr(x-y)*(1/twosigma2))-(m_x-1)*tauzeta_/(y*Zeta)+tauzeta_-(m_y-1) * c2;

return score;
}

double scoring_params::opt_size_score_simple(double x1, double x2, int m1, int m2){
if (abs(x1-x2)>3*Distr_sigma)
{
    double score=-1-Mu*(m1-1)-(m2-1)*Mu;
    return score;
}


double score=-1+2*exp(-pow((x1-x2),2)/(2*pow(Distr_sigma,2)))-Mu*(m1-1)-(m2-1)*Mu;
return score;
}

double scoring_params::LR_size_score(double x1, double x2, int m1, int m2){
  if(abs(x1-x2) > 15*sqrt(Distr_sigma)) return -100;

  double score;

  score = logGamma(m1) + logGamma(m2) + (m1+m2)*logtau
    - c3
    + (x1+x2)*(1/Tau )
    - (m1-1)*log(x1) - (m2-1)*log(double(x2))
    -sqr(x1-x2)*(1/(twosigma2));
  return score;
}

double scoring_params::LR_stretch_size_score(double x1, double x2, int m1, int m2){
  if(abs(x1-x2) > 15*sqrt(Distr_sigma)) return -100;

  double score;

  score = logGamma(m1) + logGamma(m2) + (m1+m2)*logtau
    - c3
    + (x1+x2)*(1/Tau )
    - (m1-1)*log(x1) - (m2-1)*log(double(x2))
    -sqr(x1-x2)*(1/(twosigma2+x1*Stretch_var));
  return score;
}



double scoring_params::site_score(int m1, int m2){
  assert(m1>=1 && m2>=1);
  return Nu - Nu*(m1+m2-2);

}
