#ifndef SCORING_H_INCLUDED
#define SCORING_H_INCLUDED


#ifdef WITH_MATLAB
#include "mex.h"
#endif

#ifndef __IOSTREAM__
#include <iostream>
#endif // __IOSTREAM__

#ifndef __STRING__
#include <string>
#endif

#ifndef __STDLIB__
#include <stdlib.h>     /* abs */
#endif // __STDLIB__

#ifndef _MATH_H_
#include <math.h>
#endif // _MATH_H_

#ifndef __SGI_STL_VECTOR
#include <vector>
#endif

#ifndef __FSTREAM__
#include <fstream>
#endif

#include "functions.h"

using namespace std;

class scoring_params{
private:
    void stringToUpper(string &s);
    void stringToLower(string &s);
 public:

      double Distr_sigma;
      double Distr_lambda;
      double Mu;
      double Nu;
      double Tau;
      double Zeta;
      double Theta;
	  double Stretch_var;
      int Delta;
	  int Method;

      string method;

	  //constants
        double c1;
        double c2;
		double c3;
		double logtau;
		double tauzeta;
		double twosigma2; //2*Distr_sigma^2
		double tauzeta_; //offset for fp;
		
		vector<double> missites;

      int number_of_parameters;

	  int score_thresh_fit;
 //public:
  scoring_params();

  scoring_params(ifstream& fstr);
  scoring_params(int method,mxArray *matlabArray);

  void set_parameter(string param,string value);

  void check_parameters();
  void check_parameters_fit();
  void setconstants();

  double score(double x1, double x2, int m1, int m2);
  double opt_size_score_simple(double x1, double x2, int m1, int m2);
  double LR_size_score(double x1, double x2, int m1, int m2);
  double LR_stretch_size_score(double x1, double x2, int m1, int m2);
 
  double site_score(int m1, int m2);

   double fit_score(double x1, double x2, int m1, int m2);
   double opt_fit_score_simple(double x1, double x2, int m1, int m2);
   double LR_fit_score(double x1, double x2, int m1, int m2);
   double LR_stretch_fit_score(double x1, double x2, int m1, int m2);

   double JD_norm_score(double x1, double x2, int m1, int m2);
   double JD_exp_score(double x1, double x2, int m1, int m2);
  

  //scoring_params & operator=(const scoring_params& sp);
};
#endif // SCORING_H_INCLUDED
