#ifndef MAP_READ_H_INCLUDED
#define MAP_READ_H_INCLUDED

#include <string>
#include <boost/math/special_functions/round.hpp>

#ifndef __SGI_STL_VECTOR
#include <vector>
#endif

#ifndef __FSTREAM__
#include <fstream>
#endif

#ifndef __IOSTREAM__
#include <iostream>
#endif // __IOSTREAM__

#ifndef _MATH_H_
#include <math.h>
#endif // _MATH_H_


#ifndef _GLIBCXX_SSTREAM
#include <sstream>
#endif

#ifndef __FSTREAM__
#include <fstream>
#endif

#ifndef __STRING__
#include <string>
#endif

#ifndef __STDLIB__
#include <stdlib.h>     /* abs */
#endif // __STDLIB__


#ifdef WITH_MATLAB
#include "mex.h"
#endif

using namespace std;

#define conv 1 // /0.34//107/(0.34*1.636) //1


class map_read_class{
private:

 public:

    vector<double> map_read; //stores the array of rf lengths

    map_read_class() {};
    map_read_class(vector<double> &read, int id, string name, string Ename , string Eacr);
    map_read_class(string r, int ind = 0); //reads the om_read from the lines from the file
    map_read_class(vector<double> &read);

    map_read_class reverse();
	map_read_class cutmap( int begin, int end);
	map_read_class leaveout(vector<int> removesites);
	map_read_class erasesites(vector<int> removesites);
};

class map_read_collection{
private:

 public:
  vector<map_read_class> collection;

  map_read_collection(ifstream& fstr);
  map_read_collection();
  map_read_collection(mxArray *matlabArray);
  void load_matlabarray(mxArray *matlabArray);
  void load(ifstream& fstr);

};

class reference_read_collection{
private:
public:
    vector<map_read_class> collection;

    reference_read_collection(ifstream& fstr);
    reference_read_collection(mxArray *matlabArray);
    reference_read_collection();
    void reference_convertbasepair(string sequence);
    void load_reference(ifstream& fstr);
	void load_matlabarray(mxArray *matlabArray);

    };

#endif // MAP_READ_H_INCLUDED
