# README #

This algorithm is used for aligning DNA maps to a reference map or other DNA maps.

### Summary###
* Version 1.0

The program compiles to a mex algorithm to be used in map alignment using matlab

The input needs to be of the following form:  
    SWFitter(Function,Data,referenceT7,Fitting_method,SP)    
1. **Function** is a string for the algorithm to run (such as 'fittoreference')  
2. **Data** is a 1D vector containing the map locations  
3. **Reference** is a 2D vector containing the map locations for the Reference  
4. **Fitting_method** is a string for what fitting method to use (i.e. 'LR','Simple' or 'LR_Stretch') for Likelihood Relationship, Simple fitting method and Likelihood Relationship with stretch variation  
5.  **SP**, Scoring Parameters dependent on the fitting method. Run the algorithm with an empty matrix to see the options  
	     
		 
See Fitter.m for more details  

### Compiling ###

To compile in visual studio (for 64 bits matlab):
Change the following settings:

Configuration properties -> General:

* Set Target Extension to .mexw64
* Set Configuration Type to Dynamic Library (.dll)

Configureation poperties -> VC++ Directories:

* Add $(MATLAB_ROOT)\extern\include; to Include Directories

Configuration properties -> Linker -> General:

* Add $(MATLAB_ROOT)\extern\lib\win64\microsoft; to Additional Library Directories

Configuration properties -> Linker -> Input:

* Add libmx.lib;libmex.lib;libmat.lib; to Additional Dependencies

Configuration properties -> Linker -> Command Line:

* Add /export:mexFunction to Additional Options

 $(MATLAB_ROOT) is the path to Matlab's root folder, eg. C:\Program Files\MATLAB\R2014.