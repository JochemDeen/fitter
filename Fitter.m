function [Fittingscores] = Fitter()
% FITTER is an example program to illustrate the alignment of two DNA molecules 
% obtained from high resolution microscopy images
%
%   This function wraps around SWFitter which is the external (mex)
%   function that is provided by SWFitter.mexw64 (or .mexw32). To
%   get a description of this function, simply enter SWFitter('fittoreference')
%   in the Matlab command line, and a description will be shown in the 
%   resulting error text.
%
%   Generally the function takes the following form:
%   SWFitter(Function,Data,referenceT7,Fitting_method,SP)
%       Where Function is a string for the algorithm to run (such as 'fittoreference')
%       Data is a 1D vector containing the map locations
%       Reference is a 2D vector containing the map locations for the Reference
%       Fitting_method is a string for what fitting method to use (i.e. 'LR','Simple' or 'LR_Stretch')
%           for Likelihood Relationship, Simple fitting method and Likelihood Relationship with stretch variation
%       SP, Scoring Parameters dependent on the fitting method. Run the algorithm with an empty matrix to see the options
% 
%	Fittingscores is a 2D matrix. For X maps, there will be X corresponding rows. 
%	Every row will have 5 fitting values:
% 	1. map ID
%	2. reference ID (that gave the highest score)
%	3. offset
%	4. orientation
%	5. score
%
% 	Alternatively, if only one map is supplied, the program can output the full 
%	alignment in the following format:
%	[Fittingscores, All_sites, All_scores,Full_scoring_matrix]=SWFitter(...)
% 	- All_sites 			is a row vector that displays all the sites that are correctly matched to a reference DNA 
%	- All_scores 			is a row vector that displays the score for each matched sites
%	- Full_scoring_matrix 	is a 2D matrix that contains the full scoring matrix of of N 
%							(number of sites on map) by M (number of sites on reference)

% Set fitting parameters
Distr_sigma=50; %localization inaccuracy (in bp)
Delta=10; %consecutive false positives
Tau=360; %mean fragment length (in bp)
Zeta=1/1000; %false positives per 1kbp
Theta=0.7; %labeling accuracy (between 0 and 1)
Stretch=0.05;%stretching variation (between 0 and 1)

%set the method ('Simple', 'LR' or  'LR_stretch')
method='LR_Stretch'; 

if strcmp(method,'LR_Stretch')
    SP=[Distr_sigma;Delta;Tau;Zeta;Theta;Stretch];
elseif strcmp(method,'LR')
    SP=[Distr_sigma;Delta;Tau;Zeta;Theta];
elseif strcmp(method,'Simple')
	SP=[Distr_sigma;Delta;Zeta];
end

%Load data map
load('Data.mat');

%average stretch of molecule
%stretch=1.576;
%convert molecule from nm to bp
%Data=Data./(0.34*stretch);

%Load reference map
load('referenceSA_T7.mat');

%Calling the fitting function
[Fittingscores,All_sites,All_scores,Scoring_matrix]=SWFitter('fittoreference',Data,referenceT7,method,SP);

disp(['used: ', num2str(size(All_sites,2)), ' sites of the map, with score:' num2str(Fittingscores(5)) 'and offset: ' , num2str(Fittingscores(3))])

%Calculate SNR
%all possible scores for alignment on the entire molecule
last_scores=max(Scoring_matrix(:,end-Delta:end),[],2);
figure;plot(referenceT7,last_scores);
%calculate background
background=median(last_scores);

%calculate signal=(score-back)^2
Signal=(Fittingscores(5)-background)^2;
%calculate noise=mean((Noise-back)^2)
Noise=mean((last_scores-background).^2);
%calculate SNR
SNR=Signal/Noise;

disp(['SNR: ', num2str(SNR)])




%plot score
plotscore_v2(Data,referenceT7,All_sites,All_scores,Fittingscores(4))